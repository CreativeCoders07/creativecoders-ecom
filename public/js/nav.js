const createNav = ()=>{
    let nav = document.querySelector('.navbar');
    nav.innerHTML=`
    <div class="nav">
    <img src="img/dark-logo.png" class="brand-logo" alt="">
    <div class="nav-items">
        <div class="search">
            <input type="text" class="search-box" placeholder="Search brand">
            <button class="search-btn">search</button>

        </div>
        <a href=""><img src="img/user.png" alt="">  </a>
        <a href=""><img src="img/cart.png" alt="">  </a>

    </div>
</div>
<ul class="link-container"> 
    <li class="link-item"> <a href="#" class="link">HOME</a></li>
    <li class="link-item"> <a href="#" class="link">WOMEN</a></li>
    <li class="link-item"> <a href="#" class="link">MEN</a></li>
    <li class="link-item"> <a href="#" class="link">ACCESSORIES</a></li>

</ul>
    `;
}
createNav();